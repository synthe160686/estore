using eStore.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
   .AddCookie(options =>
   {
	   options.ExpireTimeSpan = TimeSpan.FromMinutes(20);
	   options.SlidingExpiration = true;
	   options.AccessDeniedPath = "/Forbidden";
	   options.LogoutPath = "/Logout";
	   options.LoginPath = "/Login";
   });
builder.Services.AddAuthorization(options =>
{
	options.AddPolicy("Admin", policy => policy.RequireClaim("Role", "1"));
});

builder.Services.AddSession();
builder.Services.AddRazorPages();
builder.Services.AddDbContext<eStoreContext>(options =>
	options.UseSqlServer(builder.Configuration["ConnectionStrings"]));
builder.Services.AddScoped<eStoreContext>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
	app.UseExceptionHandler("/Error");
	// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
	app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseSession();
app.UseAuthentication();
app.UseAuthorization();

app.MapRazorPages();

app.Run();


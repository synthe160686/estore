using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Security.Claims;
using eStore.Models;
using System.ComponentModel.DataAnnotations;

namespace eStore.Pages
{
	public class User
	{
		[Required(ErrorMessage = "Username must be input value!")]
		public string Username { get; set; }
		[Required(ErrorMessage = "Password must be input value!")]
		public string Password { get; set; }

	}
	public class LoginModel : PageModel
	{
		[BindProperty]
		public User User { get; set; }

		private readonly eStoreContext _context;

		public LoginModel(eStoreContext context)
		{
			_context = context;
		}

		public void OnGet()
		{
		}
		public async Task<IActionResult> OnPostAsync()
		{
			var account = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("account");

			Member member = _context.Members.FirstOrDefault(m => m.Email.Equals(User.Username) && m.Password.Equals(User.Password));

			if (!string.IsNullOrEmpty(User.Username) && !string.IsNullOrEmpty(User.Password))
			{
				if (User.Username.Equals(account["username"]) && User.Password.Equals(account["password"]))
				{

					var claims = new List<Claim>()
					{
						new Claim(ClaimTypes.NameIdentifier, User.Username),
						new Claim("Role", "admin")
					};

					var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

					var authProperties = new AuthenticationProperties
					{
						AllowRefresh = true,
						IsPersistent = true
					};

					await HttpContext.SignInAsync(
							CookieAuthenticationDefaults.AuthenticationScheme,
							new ClaimsPrincipal(claimsIdentity),
							authProperties
							);

					return RedirectToPage("./Index");
				}
				else if (member != null)
				{
					var claims = new List<Claim>()
					{
						new Claim(ClaimTypes.NameIdentifier, User.Username),
						new Claim("Role", "user")
					};

					var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

					var authProperties = new AuthenticationProperties
					{
						AllowRefresh = true,
						IsPersistent = true
					};

					await HttpContext.SignInAsync(
							CookieAuthenticationDefaults.AuthenticationScheme,
							new ClaimsPrincipal(claimsIdentity),
							authProperties
							);

					return RedirectToPage("./Index");
				}
				else
				{
					ModelState.AddModelError("Error", "Wrong username or password.");
					return Page();
				}
			}
			else
			{
				ModelState.AddModelError("Error", "Please enter username and password");
				return Page();
			}
		}


	}
}

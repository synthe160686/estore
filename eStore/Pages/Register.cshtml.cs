
using eStore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace eStore.Pages
{
	public class UserRegister
	{
		[Required(ErrorMessage ="email must be input value!")]
		public string email { get; set; }
		[Required(ErrorMessage = "company must be input value!")]
		public string company { get; set; }
		[Required(ErrorMessage = "city must be input value!")]
		public string city { get; set; }
		[Required(ErrorMessage = "country must be input value!")]
		public string country { get; set; }
		[Required(ErrorMessage = "password must be input value!")]
		public string password { get; set; }

	}
	public class RegisterModel : PageModel
	{

		private readonly eStoreContext _context;
		public RegisterModel(eStoreContext context)
		{
			_context = context;
		}
		[BindProperty]
		public UserRegister UserRegister { get; set; }

		public Member Member { get; set; }

		public void OnGet()
		{
		}

		public async Task<IActionResult> OnPostAsync()
		{
			if(_context.Members.FirstOrDefault(x =>x.Email.Equals(UserRegister.email)) != null)
			{
				ModelState.AddModelError("UserRegister.username", "Email already exists");
				return Page();
			}
			if (ModelState.IsValid)
			{
				Member = new Member()
				{
					Email = UserRegister.email,
					CompanyName = UserRegister.company,
					City = UserRegister.city,
					Country = UserRegister.country,
					Password = UserRegister.password
				};
				_context.Members.Add(Member);
				try
				{
					await _context.SaveChangesAsync();
					return RedirectToPage("./Login");
				}
				catch (Exception ex)
				{
					ModelState.AddModelError("Error", ex.Message);
					return Page();
				}
			}
			return Page();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using eStore.Models;
using System.Security.Claims;

namespace eStore.Pages.Membermaster
{
	public class UpdateProfileModel : PageModel
	{
		private readonly eStoreContext _context;

		public UpdateProfileModel(eStoreContext context)
		{
			_context = context;
		}

		[BindProperty]
		public string email { get; set; }

		[BindProperty]
		public string password { get; set; }

		[BindProperty]
		public string company { get; set; }

		[BindProperty]
		public string city { get; set; }

		[BindProperty]
		public string country { get; set; }
		public string succesMessage { get; set; } = "";
		public async Task<IActionResult> OnGetAsync(int? id)
		{
			var user = HttpContext.User;
			var username = user.FindFirst(ClaimTypes.NameIdentifier)?.Value;
			var customer = await _context.Members.FirstOrDefaultAsync(m => m.Email == username);

			if (customer != null)
			{
				email = customer.Email;
				password = customer.Password;
				company = customer.CompanyName;
				city = customer.City;
				country = customer.Country;
			}

			return Page();
		}

		public async Task<IActionResult> OnPostAsync()
		{
			if (!ModelState.IsValid)
			{
				return Page();
			}

			var user = HttpContext.User;
			var username = user.FindFirst(ClaimTypes.NameIdentifier)?.Value;
			var customer = await _context.Members.FirstOrDefaultAsync(m => m.Email == email);

			if (customer != null)
			{
				customer.Password = password;
				customer.Country = country;
				customer.CompanyName = company;
				customer.City = city;
				

				try
				{
					await _context.SaveChangesAsync();
					succesMessage = "Update profile succesfull!";
					//return RedirectToPage("./UpdateProfile");
				}
				catch (Exception e)
				{
                    ModelState.AddModelError("Error", "Please enter username and password"); 
				}
			}

			return Page();
		}


	}
}
